
import logging
import coloredlogs
import os

from flask import render_template, Flask, request, flash

from flask_mongoengine import MongoEngine

from flask_security import Security, MongoEngineUserDatastore
from flask_security import UserMixin, RoleMixin
# from flask_security import login_required
# from flask_security import roles_required, current_user
# from flask_security.utils import encrypt_password

from flask_mail import Mail  # , Message

from mongoengine.queryset.visitor import Q

from operator import itemgetter
# from copy import copy
# from re import match, compile
from datetime import datetime
# from getpass import getpass

# from pubdb.repec import get_inst_authors
from pubdb.publications import update_repec
from pubdb.forms import SearchPaperForm, JelCodesForm
from pubdb.models import Article, WorkingPaper
from pubdb.reports import number_of_articles_per_year
from pubdb.reports import count_jel_codes, get_jel_classification

# logging
coloredlogs.install(
    milliseconds=True,
    level='DEBUG' if bool(os.environ.get('FLASK_DEBUG', 0)) else 'INFO')

log = logging.getLogger(__name__)

app = Flask(__name__)

app.config.from_object('pubdb.default_config')
app.config.from_object('pubdb.local_config')

mail = Mail(app)
# Create database connection object
db = MongoEngine(app)


class Role(db.Document, RoleMixin):
    name = db.StringField(max_length=80, unique=True)
    description = db.StringField(max_length=255)


class User(db.Document, UserMixin):
    email = db.StringField(max_length=255)
    password = db.StringField(max_length=255)
    active = db.BooleanField(default=True)
    confirmed_at = db.DateTimeField()
    roles = db.ListField(db.ReferenceField(Role), default=[])
    last_login_at = db.DateTimeField()
    current_login_at = db.DateTimeField()
    last_login_ip = db.StringField(max_length=255)
    current_login_ip = db.StringField(max_length=255)
    login_count = db.IntField()


# Setup Flask-Security
user_datastore = MongoEngineUserDatastore(db, User, Role)
security = Security(app, user_datastore)


@app.route('/', methods=['GET', 'POST'])
def home():
    return render_template("home.html",
                           title='Home',
                           collections=['Authors', 'Journals', 'Papers'])


@app.route("/search_papers", methods=['GET', 'POST'])
def search_papers():
    form = SearchPaperForm(request.form)

    if request.method == 'GET':
        return render_template('search_papers.html',
                               title="SearchPapers",
                               form=form,
                               results=[])
    else:
        results = query_papers(form)
        return render_template("search_papers.html", form=form,
                               results=results,
                               a_keys=[
                                   ('author', 'Authors'),
                                   ('year', 'Year'),
                                   ('title', 'Title'),
                                   ('journal_name', 'Journal'),
                                   ('volume', 'Volume'),
                                   ('issue', 'Issue'),
                                   ('pages', 'Pages'),
                                   ('jel_codes', 'JEL codes')],
                               w_keys=[
                                   ('author', 'Authors'),
                                   ('year', 'Year'),
                                   ('title', 'Title'),
                                   ('wpseries_name', 'Series'),
                                   ('number', 'Number'),
                                   ('jel_codes', 'JEL codes')
                               ])


@app.route("/report_articles_per_year")
def report_articles_per_year():
    results = number_of_articles_per_year()
    return render_template("report_articles_per_year.html", results=results)


@app.route("/report_articles_by_jel_codes", methods=['GET', 'POST'])
def report_articles_by_jel_code():
    global _min_year
    global _max_year

    if request.method == 'GET':
        form = JelCodesForm()
        return render_template("report_articles_by_jel_codes.html",
                               form=form, table=[])
    else:
        form = JelCodesForm()
        log.debug("validate?: %s", form.validate())
        log.debug("data: %s", form.data)
        log.debug("errors: %s", form.errors)

        if not form.validate():
            # watch out for python3 new syntax
            for param, errors in form.errors.items():
                for error in errors:
                    flash("{}: {}".format(param, error))

            return render_template("report_articles_by_jel_codes.html",
                                   form=form, table=[])

        # FIXME: this loads all the data from MongoDB in memory, can't we
        # delegate to MongoDB this counting?
        papers = Article.objects

        weighted = bool(form.data['weighted']) # noqa
        year1 = int(form.data['year1'])
        year2 = int(form.data['year2'])
        level = int(form.data['level'])
        
        jel_counts = count_jel_codes(papers, year1, year2, level)
        years = [str(y) for y in range(year1, year2 + 1)]
        nyears = year2 - year1 + 2
        table = []
        jel_classification = get_jel_classification()
        for code, values in jel_counts.items():
            code = code.upper()
            if code not in jel_classification:
                log.warn("Code %s is not in actual JEL classification", code)
                continue
            row = [code, jel_classification[code]] + [" "] * nyears
            total = 0
            for y, count in values.items():
                row[int(y) - year1 + 2] = "{0:.1f}".format(count)
                total += count
            row[-1] = "{0:.1f}".format(total)
            table.append(row)
            
        table = [['Code', 'Category'] +
                 years +
                 ["Total"]] + sorted(table, key=itemgetter(0))
        
        return render_template("report_articles_by_jel_codes.html",
                               form=form, table=table)


@app.cli.command()
def update_papers_repec():
    log.info("update_papers_repec: starting...")
    t1 = datetime.now()
    result = update_repec(app.config)
    tdelta = datetime.now() - t1
    if result:
        log.info("ERROR: {}".format(result))
    else:
        log.info("Elapsed time: {} seconds".format(tdelta))


def query_papers(form):
    query = Q()
    for f in form:
        if f.id == "csrf_token":
            continue
        data = f.data
        if data:
            if f.type == "SelectField":
                name = f.id
                query &= Q(**{name: data})
            else:
                for token in data.lower().split():
                    name = f.id + "__icontains"
                    query &= Q(**{name: token})
    return {
        'Article': Article.objects(query).order_by("-year"),
        'WorkingPaper': WorkingPaper.objects(query).order_by("-year")
    }
