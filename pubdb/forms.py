from flask_wtf import FlaskForm
from wtforms.fields import StringField, IntegerField
from wtforms.fields import SelectField, FormField, BooleanField
from wtforms.validators import DataRequired


class SearchPaperForm(FlaskForm):
    author = StringField("Authors")
    title = StringField("Title")
    year = StringField("Publication year")
    wpseries_name = StringField("WP series name")
    journal_name = StringField("Journal name")
    seriestype = SelectField("Type",choices=[
        ('', 'All'), ('redif-article', 'Article'),
        ('redif-paper', 'Working Paper'),
        ('redif-chapter', 'Chapter')
    ])

    
class PaperForm(FlaskForm):
    author = StringField("Authors")
    title = StringField("Title")
    year = StringField("Publication year")
    wpseries_name = StringField("WP series name")
    wpseries_handle = StringField("WP series handle")
    number = StringField("WP number")
    journal_name = StringField("Journal name")
    journal_handle = StringField("Journal handle")
    volume = IntegerField("Volume")
    issue = StringField("Issue")
    month = StringField("Month")
    pages = StringField("Pages")
    seriestype = SelectField("Type", choices=[
        ('redif-article', 'Article'),
        ('redif-paper', 'Working Paper'),
        ('redif-chapter', 'Chapter')
    ])
    jel_codes = FormField(StringField)
    link = StringField("Link")
    doi = StringField("DOI")
    handle = StringField("Handle")


class JelCodesForm(FlaskForm):
    level = SelectField("Level", choices=[
        (str(x), x) for x in range(1, 4 + 1)
    ])

    weighted = BooleanField("Weighted numbers")
    year1 = IntegerField(validators=[DataRequired()])
    year2 = IntegerField(validators=[DataRequired()])
