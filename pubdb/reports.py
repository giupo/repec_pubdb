import os
import logging

from operator import itemgetter
from lxml import etree
from collections import defaultdict
from pubdb import data
from pubdb.models import Article

log = logging.getLogger(__name__)

DATA_FILE_DIR = os.path.abspath(os.path.dirname(data.__file__))

JEL_CLASSIFICATION_XML_FILE = DATA_FILE_DIR + "/jel_classificationTree.xml"

_jel_classification = None

def get_jel_classification(jel_classification_xml_file=JEL_CLASSIFICATION_XML_FILE): # noqa
    global _jel_classification
    if _jel_classification:
        return _jel_classification
    
    log.info("Getting JEL Classifications")
    with open(jel_classification_xml_file) as f:
        tree = etree.parse(f)
    root = tree.getroot()
    _jel_classification = {}
    for e in root.iter("*"):
        log.debug("Element %s", e)
        if e.tag == "code":
            code = e.text.upper()
        if e.tag == "description":
            log.debug("Code: %s, Text: %s", code, e.text)
            _jel_classification[code] = e.text
    log.info("Done getting JEL Classifications.")
    return _jel_classification


def number_of_articles_per_year():
    freqs = Article.objects.item_frequencies('year')
    counts = sorted(freqs.items(), key=itemgetter(0))
    return counts


def count_jel_codes(papers, year1, year2, level):
    # n_jel_code = 0
    jel_counts = defaultdict(lambda: defaultdict(lambda: 0))
    for p in papers:
        year = p['year']
        if int(year) < year1 or int(year) > year2:
            continue
        # count each code only once
        codes = {c[0:level] for c in p["jel_codes"]}
        n = len(codes)
        for c in codes:
            # watch out here for python2/3 problems
            jel_counts[c][year] += 1/n
    return jel_counts


