import logging

from pubdb.models import Institution, Series, Author, WorkingPaper, Article
from pubdb.repec import get_inst_authors, get_author_record_full, get_jel_codes

log = logging.getLogger(__name__)


def update_repec(config):
    log.debug("Called update_repec with following config: %s", config)
    authors = get_inst_authors(config)
    if 'error' in authors[0]:
        return authors[0]
    for a in authors:
        record = get_author_record_full(a["shortid"], config)[0]
        Author(shortid=record["shortid"],
               name=record["name"],
               email=record["email"],
               url=record["url"],
               affiliation=set_affiliation(record),
               serieseditor=set_serieseditor(record)).save()
        if "paper" in record:
            for p in record["paper"]:
                jel_codes = jel(p["handle"], config)
                save_working_paper(p, jel_codes)
        if "article" in record:
            for p in record["article"]:
                jel_codes = jel(p["handle"], config)
                save_article(p, jel_codes)


def set_affiliation(record):
    affiliation = []
    if "affiliation" in record:
        for institution in record["affiliation"]:
            affiliation.append(Institution(name=institution["name"],
                                           share=institution["share"],
                                           handle=institution["handle"]))
    return affiliation


def set_serieseditor(record):
    serieseditor = []
    if "serieseditor" in record:
        for series in record["serieseditor"]:
            serieseditor.append(Series(name=series["name"],
                                       provider=series["provider"],
                                       handle=series["handle"]))
    return serieseditor


def save_working_paper(paper, jel_codes):
    WorkingPaper(title=paper["title"],
                 author=paper["author"],
                 link=paper["link"],
                 doi=paper["doi"],
                 handle=paper["handle"],
                 seriestype=paper["seriestype"],
                 year=paper["creationdate"],
                 wpseries_handle=paper["series"]["handle"],
                 wpseries_name=paper["series"]["name"],
                 number=paper["number"],
                 jel_codes=jel_codes).save()


def save_article(paper, jel_codes):
    Article(title=paper["title"],
            author=paper["author"],
            link=paper["link"],
            doi=paper["doi"],
            handle=paper["handle"],
            seriestype=paper["seriestype"],
            year=paper["year"],
            journal_handle=paper["series"]["handle"],
            journal_name=paper["series"]["name"],
            volume=paper["volume"],
            issue=paper["issue"],
            pages=paper["pages"],
            month=paper["month"],
            jel_codes=jel_codes).save()


def jel(handle, config):
    json = get_jel_codes(handle, config)
    if len(json) and type(json[0]) == dict and "error" in json[0].keys():
        return []
    else:
        return json
