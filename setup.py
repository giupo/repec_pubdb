#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest
        errcode = pytest.main(self.test_args)
        sys.exit(errcode)


readme = open('README.md').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

requirements = [
    'wheel',
    'flask',
    'flask_mongoengine',
    'flask_security',
    'bcrypt',
    'flask_mail',
    'requests',
    'bs4',
    'sure',
    'coloredlogs',
    'httpretty',
    'lxml'
]

test_requirements = [
    'nose',
    # 'pytest', https://github.com/pypa/setuptools/issues/196
    'pytest-cov',
]

setup(
    name='pubdb',
    version='0.0.1',
    description='REPEC_PUBDB: an application to exploit REPEC ' +
                'data inside an organization',
    long_description=readme + '\n\n' + history,
    packages=find_packages('pubdb', exclude='tests/**'),
    include_package_data=True,
    install_requires=requirements,
    license="BSD",
    zip_safe=False,
    keywords=['repec', 'pubdb'],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    cmdclass={'test': PyTest},
    test_suite='tests',
    tests_require=test_requirements,
)
