import logging

from tests.base import BaseTestDBCase

from pubdb.publications import update_repec
from pubdb.models import Author, Paper

log = logging.getLogger(__name__)


class TestPublications(BaseTestDBCase):
    def test_update_repec(self):
        self.config['REPEC_INST_HANDLE'] = 'RePEc:edi:bdfgvfr'
        update_repec(self.config)
        log.info("Author.objects.count: %s", Author.objects.count())
        log.info("Paper.objects.count: %s", Paper.objects.count())
