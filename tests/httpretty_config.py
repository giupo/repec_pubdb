# import requests
import httpretty
# import unittest
import os
from tests import test_files

TEST_FILES_DIR = os.path.abspath(os.path.dirname(test_files.__file__))


def config_httpretty():
    path = '../'
    mocked_url = "https://api.repec.org/call.cgi"
    local_urls = {
        # TODO: the following files doesn't exist in the repository.
        # "https://edirc.repec.org/data/bdfgvfr.html": TEST_FILES_DIR + "/bdfgvfr.html",
        "https://ideas.repec.org/top/old/1608": TEST_FILES_DIR + "/Rankings_1608.html",
        "https://ideas.repec.org/top/old/1608/top.afr.html": TEST_FILES_DIR + "/Rankings_afr_1607.html",
        "https://ideas.repec.org/top/old/1608/top.acc.html": TEST_FILES_DIR + "/Rankings_acc_1607.html",
    } # noqa
    
    httpretty.enable()
    for link in local_urls.keys():
        with open(local_urls[link], encoding="utf-8") as f:
            local_page = f.read()
        httpretty.register_uri(httpretty.GET,
                               link,
                               body=local_page)
    httpretty.register_uri(
        httpretty.GET,
        mocked_url,  # "https://api.repec.org/call.cgi",
        body=dummy_call_cgi)

    return mocked_url


def end_httpretty_config():
    httpretty.disable()
    httpretty.reset()


def dummy_call_cgi(request, uri, headers):
    if request.querystring == {'code': ['XXXXXXXX'], 'getinstauthors': ['RePEc:edi:bdfgvfr']}: # noqa
        with open(TEST_FILES_DIR + "/bdf.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)

    if request.querystring == {'code': ['XXXXXXXX'], 'getauthorrecordfull': ['pju1']}: # noqa
        with open(TEST_FILES_DIR + "/pju1.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)

    if request.querystring == {'code': ['XXXXXXXX'], 'getjelforitem': ['RePEc:cpm:dynare:001']}: # noqa
        with open(TEST_FILES_DIR + "/jel_code.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)
    
    if "getjelforitem" in request.querystring:
        with open(TEST_FILES_DIR + "/jel_code_error.json", encoding="utf-8") as f: 
            local_page = f.read()
        return (200, headers, local_page)

    if request.querystring == {'code': ['XXXXXXXX'], 'getref': ['RePEc:eee:ecmode:v:24:y:2007:i:3:p:481-505']}: # noqa
        with open(TEST_FILES_DIR + "/paper.json", encoding="utf-8") as f:
            local_page = f.read()
        return (200, headers, local_page)

    return (404, headers, "not found")
    
