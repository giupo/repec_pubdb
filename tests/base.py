import unittest
import httpretty
import coloredlogs

from tests.httpretty_config import config_httpretty
from mongoengine import connect


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        super().setUp()
        config_httpretty()
        # logging.basicConfig(
        #    handlers=[logging.FileHandler("pubdb_test.log", 'w', 'utf-8')],
        # level=logging.DEBUG)
        coloredlogs.install(level='DEBUG', milliseconds=True)
        
    def tearDown(self):
        httpretty.disable()
        httpretty.reset()


class BaseTestDBCase(unittest.TestCase):
    
    def setUp(self):
        super().setUp()
        # use/create new test database
        connect("pubdb_test", host="mongomock://localhost")
        # call httpretty after calling mongo client
        self.config = {
            'REPEC_API_CODE': 'XXXXXXXX',
            'REPEC_API_URL': config_httpretty()
        }
        # config_httpretty()
        # logging.basicConfig(
        #     handlers=[logging.FileHandler("pubdb_test.log", 'w', 'utf-8')],
        #     level=logging.DEBUG)
        coloredlogs.install(level='DEBUG', milliseconds=True)
        
    def tearDown(self):
        httpretty.disable()
        # self.test_backend.mongo_client.close()
