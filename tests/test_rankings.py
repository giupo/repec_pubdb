import logging
import unittest

from tests.base import BaseTestDBCase
from pubdb.models import Rankings

log = logging.getLogger(__name__)


class TestRankings(BaseTestDBCase):
    def test_update_rankings(self):
        # FIXME
        try:
            from pubdb.rankings import update_rankings
        except ImportError:
            unittest.skip("pubdb.rankings doesn't exists")
            return

        
        update_rankings()
        log.info("Rankings.objects.count(): %s", Rankings.objects.count())
