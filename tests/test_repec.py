# import os
import httpretty
# import requests

from pubdb.repec import get_inst_authors, get_author_record_full
from pubdb.repec import get_ranking_fields, get_jel_codes, get_ref
# from pubdb.repec import REPEC_LAST_RANKINGS_URL

from tests.base import BaseTestCase

from sure import expect
# from json import loads

REPEC_RANKINGS_URL = "https://ideas.repec.org/top/old/1608"

class test_repec_api(BaseTestCase):
    def test_get_inst_authors(self):
        httpretty.enable()
        httpretty.register_uri(httpretty.GET,
                               "https://api.repec.org/call.cgi",
                               body='[{"return": "OK"}]')
        
        a = get_inst_authors('XXXXXXXX','bdfgvfr')
        expect(httpretty.last_request()).to.have.property("querystring").being.equal({
            "code": ["XXXXXXXX"],
            "getinstauthors": ["RePEc:edi:bdfgvfr"],
        })
        httpretty.disable()

    def test_get_author_record_full(self):
        httpretty.enable()
        httpretty.register_uri(httpretty.GET,
                               "https://api.repec.org/call.cgi",
                               body='[{"return": "OK"}]')
        
        a = get_author_record_full('XXXXXXXX','pju1')
        expect(httpretty.last_request()).to.have.property("querystring").being.equal({
            "code": ["XXXXXXXX"],
            "getauthorrecordfull": ["pju1"],
        })
        httpretty.disable()

    def test_get_inst_authors_json(self):
        json = get_inst_authors('XXXXXXXX','bdfgvfr')
        self.assertIs(type(json),list)

    def test_get_author_record_full(self):
        json = get_author_record_full('XXXXXXXX','pju1')
        self.assertIs(type(json),list)
    
    def test_get_jel_codes(self):
        json = get_jel_codes('XXXXXXXX','RePEc:cpm:dynare:001')
        self.assertIs(type(json),list)
    
    def test_get_jel_codes_error(self):
        json = get_jel_codes('XXXXXXXX','RePEc:eee:ecmode:v:24:y:2007:i:3:p:481-505')
        self.assertIs(type(json),list)
    
    def test_get_ref(self):
        json = get_ref('XXXXXXXX','RePEc:eee:ecmode:v:24:y:2007:i:3:p:481-505')
        self.assertIs(type(json),list)


class test_repec_rankings(BaseTestCase):
    def test_get_ranking_fields(self):
        r = get_ranking_fields(REPEC_RANKINGS_URL)
        self.assertEqual(len(r),90)
        self.assertEqual(r[0]["id"],"ACC")
        self.assertEqual(r[-1]["id"],"URE")
        self.assertEqual(r[0]["url"],"https://ideas.repec.org/top/old/1608/top.acc.html")
        self.assertEqual(r[-1]["url"],"https://ideas.repec.org/top/old/1608/top.ure.html")
